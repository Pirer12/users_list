/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
} from 'react-native';
import { Icon} from 'react-native-elements'
import UsersList from './components/UsersList';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      data: [],
      fetching_from_server: false,
      page:1,
      total_pages:0
    };
  }

  render() {
    return (
      <>
        <SafeAreaView testID='home'>
          <View style={styles.top}>
            <View style={styles.topWrapper}>
              <Icon name='ios-list-box' type="ionicon" color="white" size={70}/>
              <Text style={{fontSize:25,color:'white'}}>Users List</Text>
            </View>
          </View>
          <ScrollView style={styles.bodyList} testID='usersListComponent'>
            <View>
              <UsersList itemsPerPage={5} onPageChange/>
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  };
}

const styles = StyleSheet.create({
  top:{
    height:280,
    backgroundColor:'#0066ff'
  },
  topWrapper:{
    flex:1,
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent:'center'
  },
  bodyList:{
    height:600,
    paddingTop:10,
    marginTop:-20,
    borderTopLeftRadius:20,
    borderTopRightRadius:20,
    borderWidth:10,
    backgroundColor:'white',
    borderColor:'white'
  }
});
