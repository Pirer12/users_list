describe('Add user', () => {

  it('should have home screen', async () => {
    await expect(element(by.id('home'))).toBeVisible();
  });

  it('should show users list', async () => {
    await expect(element(by.id('usersList'))).toBeVisible();
  });

  it('should scroll and get all elements', async () => {
    await element(by.id('usersListComponent')).swipe('up');
  })

  it('should have add button', async () => {
    await expect(element(by.id('addBtn'))).toBeVisible();
  });

  it('should open modal', async () => {
    await element(by.id('addBtn')).tap();
  });

  it('should fill inputs sample data', async () => {
    await element(by.id('firstName_input')).typeText('JohnyTest');
    await element(by.id('lastName_input')).typeText('Bravo');
    await element(by.id('email_input')).typeText('johnybravo@mail.com');
  })

  it('should add user', async () => {
    await element(by.id('addUser')).tap();
    await element(by.id('addUser')).tap();
  });

  it('should be on the list',async () => {
    await element(by.id('usersListComponent')).swipe('up');
    await expect(element(by.text('JohnyTest'))).toHaveText('JohnyTest');
  })

});
