describe('Get all users', () => {

  it('should have home screen', async () => {
    await expect(element(by.id('home'))).toBeVisible();
  });

  it('should show users list', async () => {
    await expect(element(by.id('usersList'))).toBeVisible();
  });

  it('should scroll and get all elements', async () => {
    await element(by.id('usersListComponent')).swipe('up');
  })

  it('should show 3 in page number', async () => {
    await expect(element(by.id('pageInfo'))).toHaveLabel('You are on page 3');
  })

});
