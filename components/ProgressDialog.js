import React from 'react';
import { Modal, View, StyleSheet, Text, ActivityIndicator } from 'react-native';

const ProgressDialog = ({ visible }) => (
    <Modal
        transparent={true}
        visible={visible}
    >
        <View style={styles.container}>
            <View style={styles.content}>
                <View style={styles.loading}>
                    <View style={styles.loader}>
                        <ActivityIndicator size="large" />
                    </View>
                    <View style={styles.loadingContent}>
                        <Text>Loading...</Text>
                    </View>
                </View>
            </View>
        </View>
    </Modal>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, .5)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    content: {
        padding: 30,
        backgroundColor: 'white',
        borderRadius: 15
    },
    title: {
        fontSize: 14,
        marginBottom: 20,
        fontWeight: 'bold',
    },
    loading: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    loadingContent: {
        fontSize: 14,
        paddingHorizontal: 15,
    }
})

export default ProgressDialog;
