import React, {Component} from 'react';
import { Modal, View, StyleSheet, Text,FlatList,TouchableHighlight,Alert } from 'react-native';
import { ListItem,Overlay,Icon,Button,Input } from 'react-native-elements'
import { userAdd,getUser } from '../services/provider';
import ProgressDialog from './ProgressDialog';

export default class UsersList extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            data: [],
            fetching_from_server: false,
            page:1,
            total_pages:0,
            modalVisible: false,
            firstName: '',
            lastName: '',
            email: ''
        }
    }

    componentDidMount(){
        getUser(this.state.page,this.props.itemsPerPage).then((res)=>{
          this.setState({
            data:res.data,
            page:res.page,
            total_pages:res.total_pages
          })
        })
    }

    getMore(){
        if(this.state.page+1 <= this.state.total_pages)
        getUser(this.state.page+1,this.props.itemsPerPage).then((res)=>{
          this.setState({
            data:this.state.data.concat(res.data),
            page:res.page,
            total_pages:res.total_pages
          })
        })
    }
    
    setModalVisible() {
        this.setState({modalVisible: !this.state.modalVisible});
    }
    
    userAddFun(){
        this.setState({loading:true})
        setTimeout(() => {
            if(this.state.email != '' || this.state.firstName != '' || this.state.lastName != ''){
                let user = {
                    "avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/russoedu/128.jpg",
                    "email": this.state.email,
                    "first_name": this.state.firstName,
                    "id": 10,
                    "last_name": this.state.lastName,
                }
                this.state.data.push(user)
                this.setModalVisible()
                this.setState({loading:false})
            }else{
                this.setState({loading:false})
                Alert.alert(
                    'Inputs are empty',
                    '',
                    [
                      {text: 'OK'},
                    ],
                    {cancelable: false},
                  );
            }
        }, 1500);
    }

    keyExtractor = (item, index) => index.toString()

    renderItem = ({ item }) => (
        <ListItem
            title={item.first_name}
            subtitle={item.last_name}
            leftAvatar={{
                source: item.avatar && { uri: item.avatar }
            }}
            bottomDivider
        />
    )

    render(){
        return(
            <View>
                <Icon
                    reverse
                    raised
                    name='ios-add'
                    type='ionicon'
                    color='#0066ff'
                    size={30}
                    containerStyle={{position:'absolute',right:5,bottom:35,zIndex:99}}
                    onPress={()=>this.setModalVisible()}
                    testID='addBtn'
                />
                <Text style={styles.pageNumber} testID='pageInfo'>You are on page {this.state.page}</Text>
                    <FlatList
                        keyExtractor={this.keyExtractor}
                        data={this.state.data}
                        renderItem={this.renderItem}
                        marginBottom={0}
                        paddingBottom={1}
                        onEndReachedThreshold={.1}
                        onEndReached={()=>this.getMore()}
                        style={styles.listStyle}
                        testID='usersList'
                    />
                <Overlay
                    animationType="slide"
                    transparent={true}
                    overlayStyle={{borderRadius:20}}
                    visible={this.state.modalVisible}
                    width="70%"
                    height={400}
                    >
                    <ProgressDialog visible={this.state.loading}/>
                    <View style={{flex:1,marginTop: 22}}>
                        <View style={{flex:0.8,flexDirection:'column',alignItems:'center',justifyContent:'flex-start'}}>
                            <Text style={{textAlign:'center',paddingVertical:20,fontSize:24,fontStyle:'italic'}}>Add new user</Text>
                                <Input
                                    placeholder='Type first name'
                                    onChangeText={(text)=>this.setState({firstName: text})}
                                    inputContainerStyle={styles.inputModal}
                                    inputStyle={{paddingLeft:15}}
                                    autoCorrect={false}
                                    testID="firstName_input"
                                />
                                <Input
                                    placeholder='Type last name'
                                    onChangeText={(text)=>this.setState({lastName: text})}
                                    inputContainerStyle={styles.inputModal}
                                    inputStyle={{paddingLeft:15}}
                                    autoCorrect={false}
                                    testID="lastName_input"
                                />
                                <Input
                                    placeholder='Type email'
                                    onChangeText={(text)=>this.setState({email: text})}
                                    inputContainerStyle={styles.inputModal}
                                    inputStyle={{paddingLeft:15}}
                                    autoCorrect={false}
                                    textContentType={'emailAddress'}
                                    keyboardType={'email-address'}
                                    testID="email_input"
                                />
                        </View>
                        <View style={{flex:0.5,flexDirection:'row',alignItems:'center',justifyContent:'space-between',paddingHorizontal:10}}>
                            <Button
                                title="Cancel"
                                buttonStyle={{borderRadius:40,borderWidth:1,width:120}}
                                titleStyle={{fontSize:20,paddingBottom:5}}
                                onPress={()=>this.setModalVisible()}
                            />
                            <Button
                                title="Add"
                                buttonStyle={{borderRadius:40,borderWidth:1,width:120}}
                                titleStyle={{fontSize:20,paddingBottom:5}}
                                onPress={()=>this.userAddFun()}
                                testID="addUser"
                            />
                        </View>
                    </View>
                </Overlay>
            </View>
        )
    }
}


const styles = StyleSheet.create({
  listStyle:{
    height:500,
  },
  pageNumber:{
    textAlign:'center',
    fontSize:22,
  },
  modalTitle:{
    fontSize:25,
    textAlign:'center'
  },
  inputModal:{
    borderRadius:20,
    borderWidth:1,
    height:50,
    marginBottom:10
  }
})
