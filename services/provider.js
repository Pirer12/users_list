
const api = 'https://reqres.in/api/users'

export const getUser = (page,items_per_page) => {
    return fetch(`${api}?per_page=${items_per_page}&page=${page}`,{
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
    })
    .then((res)=>res.json())
    .then(json => {
        console.log(json,'json')
        return json;
    })
    .catch(error => console.log(error));
}

export const userAdd = () => {
    let user = {
        'first_name':'test',
        'last_name': 'test_last_name',
        'email':'testEmail@mail.com'
    }
        return fetch(`${api}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({user})
        })
        .then((res)=>res.json())
        .then(json => {
            console.log(json,'json')
            return json;
        })
        .catch(error => console.log(error));
}
